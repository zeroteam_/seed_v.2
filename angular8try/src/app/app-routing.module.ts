import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TutorialAddComponent } from './tutorial-add/tutorial-add.component';
import { TutorialEditComponent } from './tutorial-edit/tutorial-edit.component';
import { TutorialGetComponent } from './tutorial-get/tutorial-get.component';

const routes: Routes = [
  {
    path: 'tutorial/create',
    component: TutorialAddComponent
  },
  {
    path: 'edit/:id',
    component: TutorialEditComponent
  },
  {
    path: 'tutorial',
    component: TutorialGetComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
