import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TutorialAddComponent } from './tutorial-add/tutorial-add.component';
import { TutorialGetComponent } from './tutorial-get/tutorial-get.component';
import { TutorialEditComponent } from './tutorial-edit/tutorial-edit.component';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TutorialsService } from './tutorials.service';

@NgModule({
  declarations: [
    AppComponent,
    TutorialAddComponent,
    TutorialGetComponent,
    TutorialEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SlimLoadingBarModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [TutorialsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
