import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { TutorialsService } from '../tutorials.service';

@Component({
  selector: 'app-tutorial-add',
  templateUrl: './tutorial-add.component.html',
  styleUrls: ['./tutorial-add.component.css']
})
export class TutorialAddComponent implements OnInit {

  angForm: FormGroup;
  constructor(private fb: FormBuilder, private ps: TutorialsService) {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      TutorialName: ['', Validators.required ],
      TutorialText: ['', Validators.required ],
      IDquestion: ['', Validators.required ]
    });
  }

  addTutorial(TutorialName, TutorialText, IDquestion) {
    this.ps.addTutorial(TutorialName, TutorialText, IDquestion);
  }

  ngOnInit() {
  }

}
