import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TutorialsService {

  uri = 'http://localhost:4000/tutorials';

  constructor(private http: HttpClient) { }

  addTutorial(TutorialName, TutorialText, IDquestion){
    const obj = {
      TutorialName,
      TutorialText,
      IDquestion
    };
    console.log(obj);
    this.http.post(`${this.uri}/add`, obj)
        .subscribe(res => console.log('Done'));
  }
}
